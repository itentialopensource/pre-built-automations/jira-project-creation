<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [Atlassian - Jira - REST](https://gitlab.com/itentialopensource/pre-built-automations/atlassian-jira-rest)

<!-- Update the below line with your Pre-Built name -->
# Jira Project Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview
This Pre-Built integrates with the [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira) to create a project within Jira. This Pre-Built may be leveraged if a process consistently requires creating a new project within Jira.

## Operations Manager and JSON-Form
This workflow has an [Operations Manager Item](./bundles/automations/Jira%20Project%20Creation.json) that calls a workflow. The Ops Manager Item uses a [JSON Form](./bundles/json_forms/Jira%20Project%20Creation.json) to specify common fields populated when a Project is created. The workflow the Ops Manager item calls queries data from the formData job variable.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`
- [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira)
  - `^1.7.2`

## Requirements

This Pre-Built requires the following:

- A [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira).
- A Jira account that has permission to create an issue.


## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows user to create a Jira Project and define the following attributes via form for the project: 
  * name of the project
  * project type (business or software)
  * project template key, which denotes the project template
  * abbreviated name of the project 
  * description of the project
  * project lead for the project
  * the default assignee for the project

<!--Jira documentation link for reference of inputs https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-projects/-->

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.


## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Item `Jira Project Creation` or call [Jira Project Creation](./bundles/workflows/Jira%20Project%20Creation.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "name": "The name of the Jira Project",
  "projectType": "The Jira Project type, which dictates the application-specific feature set: software, service_desk or business",
  "projectTemplateKey": "A prebuilt configuration for a project",
  "abbreviatedName": "The Jira Project key",
  "description": "A brief description of the Jira Project",
  "leadAccountId": "The account ID of the Jira Project lead",
  "assigneeType": "The default assignee when creating issues for this Jira Project"
}
```


## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
