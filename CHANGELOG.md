
## 0.0.19 [07-11-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/jira-project-creation!10

2024-07-11 14:40:36 +0000

---

## 0.0.18 [05-26-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/jira-project-creation!8

---

## 0.0.17 [06-21-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/jira-project-creation!7

---

## 0.0.16 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/jira-project-creation!6

---

## 0.0.15 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/jira-project-creation!5

---

## 0.0.14 [07-19-2021]

* Update bundles/json_forms/Jira Project Creation.json, package.json, README.md files

See merge request itentialopensource/pre-built-automations/jira-project-creation!4

---

## 0.0.13 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/jira-project-creation!1

---

## 0.0.12 [05-05-2021]

* Update images/jira_project_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-project-creation!9

---

## 0.0.11 [03-23-2021]

* Update images/jira_project_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-project-creation!9

---

## 0.0.10 [03-02-2021]

* Update images/jira_project_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-project-creation!9

---

## 0.0.9 [03-02-2021]

* patch/2021-03-02T08-37-58

See merge request itential/sales-engineer/selabprebuilts/jira-project-creation!8

---

## 0.0.8 [02-25-2021]

* Upload New File

See merge request itential/sales-engineer/selabdemos/create-projectjira!7

---

## 0.0.7 [02-24-2021]

* Upload New File

See merge request itential/sales-engineer/selabdemos/create-projectjira!7

---

## 0.0.6 [02-24-2021]

* Upload New File

See merge request itential/sales-engineer/selabdemos/create-projectjira!7

---

## 0.0.5 [02-24-2021]

* images

See merge request itential/sales-engineer/selabdemos/create-projectjira!5

---

## 0.0.4 [02-24-2021]

* images

See merge request itential/sales-engineer/selabdemos/create-projectjira!5

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-projectjira!1

---

## 0.0.2 [01-29-2021]

* Bug fixes and performance improvements

See commit 3f01000

---\n\n
